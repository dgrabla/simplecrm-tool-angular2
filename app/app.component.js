"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var customer_service_1 = require('./customer.service');
var router_1 = require('@angular/router');
var AppComponent = (function () {
    function AppComponent(customerService, router) {
        this.customerService = customerService;
        this.router = router;
    }
    AppComponent.prototype.add = function () {
        var _this = this;
        this.customerService.create().then(function (res) {
            console.log(res);
            var link = ['/detail', res.id];
            _this.router.navigate(link);
        });
    };
    ;
    AppComponent = __decorate([
        core_1.Component({
            selector: 'my-app',
            template: "\n<nav class=\"navbar navbar-inverse\">\n  <a class=\"navbar-brand\" href=\"#\">CRM Demo</a>\n  <ul class=\"nav navbar-nav\">\n    <li class=\"active\">\n      <a routerLink=\"/customers\">Home</a>\n    </li>\n    <li class=\"dropdown\">\n      <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\">Options <span class=\"caret\"></span></a>\n      <ul class=\"dropdown-menu\">\n        <li>\n          <a href='#' (click)=\"add();\">\n            New Customer\n          </a>\n        </li>\n        <li role=\"separator\" class=\"divider\"></li>\n        <li class='more-options'>More Options</li>\n        <li role=\"separator\" class=\"divider\"></li>\n      </ul>\n    </li>\n  </ul>\n</nav>\n\n<h1>\n  {{ title }}\n</h1> \n<router-outlet></router-outlet>\n\n<footer>\n  CRM Demo v0.001\n</footer>\n"
        }), 
        __metadata('design:paramtypes', [customer_service_1.CustomerService, router_1.Router])
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;
//# sourceMappingURL=app.component.js.map