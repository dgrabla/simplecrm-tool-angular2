import { Component } from '@angular/core';
import { CustomerService } from './customer.service';
import { Router } from '@angular/router';

@Component({
selector: 'my-app',
template: `
<nav class="navbar navbar-inverse">
  <a class="navbar-brand" href="#">CRM Demo</a>
  <ul class="nav navbar-nav">
    <li class="active">
      <a routerLink="/customers">Home</a>
    </li>
    <li class="dropdown">
      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Options <span class="caret"></span></a>
      <ul class="dropdown-menu">
        <li>
          <a href='#' (click)="add();">
            New Customer
          </a>
        </li>
        <li role="separator" class="divider"></li>
        <li class='more-options'>More Options</li>
        <li role="separator" class="divider"></li>
      </ul>
    </li>
  </ul>
</nav>

<h1>
  {{ title }}
</h1> 
<router-outlet></router-outlet>

<footer>
  CRM Demo v0.001
</footer>
`
})


export class AppComponent { 

constructor(
private customerService: CustomerService,
private router: Router
) { }

add(): void {
this.customerService.create().then(res => {
console.log(res)
let link = ['/detail', res.id];
this.router.navigate(link);
});
};



}
