import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';
import { HttpModule }    from '@angular/http';

// Imports for loading & configuring the in-memory web api
import { InMemoryWebApiModule } from 'angular2-in-memory-web-api';
import { InMemoryDataService }  from './in-memory-data.service';

import { routing } from './app.routing';

import { AppComponent }  from './app.component';
import { CustomerDetailComponent } from './customer-detail.component';
import { CustomersComponent }  from './customers.component';
import { CustomerService } from './customer.service';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    InMemoryWebApiModule.forRoot(InMemoryDataService),
    routing
  ],
  providers: [CustomerService],
  declarations: [
    AppComponent,
    CustomersComponent,
    CustomerDetailComponent
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
