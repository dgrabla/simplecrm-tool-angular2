"use strict";
var router_1 = require('@angular/router');
var customers_component_1 = require('./customers.component');
var customer_detail_component_1 = require('./customer-detail.component');
var appRoutes = [
    {
        path: '',
        component: customers_component_1.CustomersComponent
    },
    {
        path: 'detail/:id',
        component: customer_detail_component_1.CustomerDetailComponent
    }
];
exports.routing = router_1.RouterModule.forRoot(appRoutes);
//# sourceMappingURL=app.routing.js.map