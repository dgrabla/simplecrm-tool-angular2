import { ModuleWithProviders }  from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CustomersComponent }      from './customers.component';
import { CustomerDetailComponent }      from './customer-detail.component';

const appRoutes: Routes = [
  {
    path: '',
    component: CustomersComponent
    },
 {
  path: 'detail/:id',
  component: CustomerDetailComponent
}
  ];
  
export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);

