"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var customer_service_1 = require('./customer.service');
var CustomerDetailComponent = (function () {
    function CustomerDetailComponent(customerService, route) {
        this.customerService = customerService;
        this.route = route;
    }
    CustomerDetailComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.forEach(function (params) {
            var id = params['id'];
            _this.customerService.getCustomer(id)
                .then(function (customer) { return _this.customer = customer; });
        });
    };
    CustomerDetailComponent.prototype.goBack = function () {
        window.history.back();
    };
    CustomerDetailComponent.prototype.saveCustomer = function () {
        this.customerService.update(this.customer).then(this.goBack);
    };
    CustomerDetailComponent = __decorate([
        core_1.Component({
            selector: 'my-customer-detail',
            template: "\n<div *ngIf=\"customer\"> \n  <h2>Edit: {{customer.name}} <span class='customerHeaderDetails'>#{{customer.id}} {{customer.createdOn}}</span></h2>\n  <div>\n    <label>name: </label>\n    <input [(ngModel)]=\"customer.name\" placeholder=\"name\"/>\n  </div>\n\n  <div>\n    <label>email: </label>\n    <input [(ngModel)]=\"customer.email\" placeholder=\"email\"/>\n  </div>\n\n  <div>\n    <label>phone: </label>\n    <input [(ngModel)]=\"customer.phone\" placeholder=\"phone\"/>\n  </div>\n\n  <div class='input-group'>\n    <label>company: </label>\n    <input [(ngModel)]=\"customer.company\" placeholder=\"company\"/>\n  </div>\n\n</div>\n\n<button \n  (click)=\"saveCustomer()\" class='btn btn-primary' >Save</button>\n\n<button (click)=\"goBack()\" class='btn'>Cancel</button>\n"
        }), 
        __metadata('design:paramtypes', [customer_service_1.CustomerService, router_1.ActivatedRoute])
    ], CustomerDetailComponent);
    return CustomerDetailComponent;
}());
exports.CustomerDetailComponent = CustomerDetailComponent;
//# sourceMappingURL=customer-detail.component.js.map