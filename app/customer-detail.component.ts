import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';

import { CustomerService } from './customer.service';
import { Customer } from './customer';

@Component({
selector: 'my-customer-detail',
template: `
<div *ngIf="customer"> 
  <h2>Edit: {{customer.name}} <span class='customerHeaderDetails'>#{{customer.id}} {{customer.createdOn}}</span></h2>
  <div>
    <label>name: </label>
    <input [(ngModel)]="customer.name" placeholder="name"/>
  </div>

  <div>
    <label>email: </label>
    <input [(ngModel)]="customer.email" placeholder="email"/>
  </div>

  <div>
    <label>phone: </label>
    <input [(ngModel)]="customer.phone" placeholder="phone"/>
  </div>

  <div class='input-group'>
    <label>company: </label>
    <input [(ngModel)]="customer.company" placeholder="company"/>
  </div>

</div>

<button 
  (click)="saveCustomer()" class='btn btn-primary' >Save</button>

<button (click)="goBack()" class='btn'>Cancel</button>
`
})

export class CustomerDetailComponent implements OnInit {
customer: Customer;

ngOnInit(): void {
this.route.params.forEach((params: Params) => {
let id = params['id'];
this.customerService.getCustomer(id)
.then(customer => this.customer = customer);
});
}

goBack(): void {
window.history.back();
}

saveCustomer(): void {
  this.customerService.update(this.customer).then(this.goBack);
}

constructor(
private customerService: CustomerService,
private route: ActivatedRoute) {
}

}
