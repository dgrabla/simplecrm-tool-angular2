import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';

import { Customer } from './customer';

import 'rxjs/add/operator/toPromise';

@Injectable()
export class CustomerService {
  private customersUrl = 'app/customers';  // URL to web api

  constructor(private http: Http) { }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }

  getCustomers(): Promise<Customer[]> {
    return this.http.get(this.customersUrl)
               .toPromise()
               .then(response => response.json().data as Customer[])
               .catch(this.handleError);
  }
  
  getCustomer(id: string): Promise<Customer> {
    return this.getCustomers().then(customers => customers.find(customer => customer.id === id));
    }

  private headers = new Headers({'Content-Type': 'application/json'});

  update(customer: Customer): Promise<Customer> {
    const url = `${this.customersUrl}/${customer.id}`;
    customer.modifiedOn = new Date();
    console.log(customer);
    return this.http
    .put(url, JSON.stringify(customer), {headers: this.headers})
    .toPromise()
    .then(() => customer)
    .catch(this.handleError);
  }

  create(): Promise<Customer> {
  var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
    var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
    return v.toString(16);
  });

  return this.http
    .post(this.customersUrl, JSON.stringify({id: uuid}), {headers: this.headers})
    .toPromise()
    .then(res => res.json().data )
    .catch(this.handleError);
  }

  delete(id: string): Promise<void> {
  let url = `${this.customersUrl}/${id}`;
  return this.http.delete(url, {headers: this.headers})
    .toPromise()
    .then(() => null)
    .catch(this.handleError);
}

}
