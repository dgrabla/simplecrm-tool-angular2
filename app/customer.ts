export class Customer {
  id: string;
  name: string;
  company: string;
  email: string; // DGB 2016-09-13 10:52 has Typescript an email type?
  phone: string; // DGB 2016-09-13 10:52 Ideally should have its own type so we prevent multiple phone formats
  createdOn: Date; 
  modifiedOn: Date; 
}
