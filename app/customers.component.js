"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var customer_service_1 = require('./customer.service');
var CustomersComponent = (function () {
    function CustomersComponent(router, customerService) {
        this.router = router;
        this.customerService = customerService;
        this.title = 'Customer Overview';
    }
    CustomersComponent.prototype.ngOnInit = function () {
        this.getCustomers();
    };
    CustomersComponent.prototype.onSelect = function (customer) {
        var link = ['/detail', customer.id];
        this.router.navigate(link);
    };
    ;
    CustomersComponent.prototype.getCustomers = function () {
        var _this = this;
        this.customerService.getCustomers().then(function (customers) { return _this.customers = customers; });
    };
    ;
    CustomersComponent.prototype.delete = function (customer) {
        var _this = this;
        this.customerService
            .delete(customer.id)
            .then(function () {
            _this.customers = _this.customers.filter(function (h) { return h !== customer; });
        });
    };
    CustomersComponent = __decorate([
        core_1.Component({
            selector: 'my-customers',
            template: "\n<h1>\n  {{ title }}\n</h1> \n<table class='table'>\n  <tr>\n    <th>ID</th>\n    <th>Customer name</th>\n    <th>Company</th>\n    <th>Email</th>\n    <th>Fon</th>\n    <th>Created</th>\n    <th>Modified</th>\n    <th>Options</th>\n  </tr>\n  <tr *ngFor=\"let customer of customers\">\n    <td>{{ customer.id }} </td>\n    <td>{{ customer.name }}</td>\n    <td>{{ customer.company }}</td>\n    <td>{{ customer.email }}</td>\n    <td>{{ customer.phone }}</td>\n    <td>{{ customer.createdOn }}</td>\n    <td>{{ customer.modifiedOn }}</td>\n    <td><button class='btn' (click)=\"onSelect(customer)\">\u270E</button>\n      <button class=\"btn delete\"\n        (click)=\"delete(customer); $event.stopPropagation()\">\u2716</button>\n\n\n    </td>\n  </tr>\n</table>\n"
        }), 
        __metadata('design:paramtypes', [router_1.Router, customer_service_1.CustomerService])
    ], CustomersComponent);
    return CustomersComponent;
}());
exports.CustomersComponent = CustomersComponent;
//# sourceMappingURL=customers.component.js.map