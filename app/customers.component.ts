import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { CustomerService } from './customer.service';
import { CustomerDetailComponent } from './customer-detail.component';
import { Customer } from './customer';


@Component({
selector: 'my-customers',
template: `
<h1>
  {{ title }}
</h1> 
<table class='table'>
  <tr>
    <th>ID</th>
    <th>Customer name</th>
    <th>Company</th>
    <th>Email</th>
    <th>Fon</th>
    <th>Created</th>
    <th>Modified</th>
    <th>Options</th>
  </tr>
  <tr *ngFor="let customer of customers">
    <td>{{ customer.id }} </td>
    <td>{{ customer.name }}</td>
    <td>{{ customer.company }}</td>
    <td>{{ customer.email }}</td>
    <td>{{ customer.phone }}</td>
    <td>{{ customer.createdOn }}</td>
    <td>{{ customer.modifiedOn }}</td>
    <td><button class='btn' (click)="onSelect(customer)">✎</button>
      <button class="btn delete"
        (click)="delete(customer); $event.stopPropagation()">✖</button>


    </td>
  </tr>
</table>
`

})

export class CustomersComponent { 
title = 'Customer Overview';
selectedCustomer : Customer;
customers : Customer[];

ngOnInit(): void {
this.getCustomers();
}

constructor(
private router: Router,
private customerService: CustomerService) { }

onSelect(customer: Customer): void {
let link = ['/detail', customer.id];
this.router.navigate(link);
};

getCustomers(): void {
this.customerService.getCustomers().then(customers => this.customers = customers);
};

delete(customer: Customer): void {
this.customerService
.delete(customer.id)
.then(() => {
this.customers = this.customers.filter(h => h !== customer);
});
}


}
