"use strict";
var InMemoryDataService = (function () {
    function InMemoryDataService() {
    }
    InMemoryDataService.prototype.createDb = function () {
        var customers = [
            { id: '1f68a118-7b2e-11e6-a831-3c970e65d0d8', name: 'Mr. Nice', company: 'Deutsche Bahn', email: 'dd@asd.com', phone: '+4972349878' },
            { id: '26e80e38-7b2e-11e6-87e1-3c970e65d0d8', name: 'Mr. White', company: 'Deutsche Post', email: 'nospam@post.de', phone: '+492131234' },
            { id: '76712334-7b2f-11e6-87b6-3c970e65d0d8', name: 'Mr. Evil', company: 'Deutsche Strom' },
            { id: '7sdfkjsf-7b2f-11e6-960e-3c970e65d0d8', name: 'Herr Rose', company: 'Deutsche Wasser' },
            { id: '334989sd-7b2f-11e6-960e-3c970e65d0d8', name: 'Herr Rot', company: 'Deutsche Luft' },
            { id: '02139fkl-7b2f-11e6-960e-3c970e65d0d8', name: 'Herr Gelb', company: 'Deutsche Essen' }
        ];
        return { customers: customers };
    };
    return InMemoryDataService;
}());
exports.InMemoryDataService = InMemoryDataService;
//# sourceMappingURL=in-memory-data.service.js.map