# CRM Basic App / Angular2

Simple CRUD app to manage customers. Only frontend side. 

![Screencast](./screencast.gif)

DISCLAIMER:
This is my first Angular2 app and the first time I wrote Typescript. It was useful and
fun to do this to learn a little bit about the framework. I really like
Typescript: Errors at compilation are very useful. The bad side is that errors that are not catch at
compilation time are complicated to debug on the browser - Angular returns very
cryptic error messages.

## Install

Angular2 requires node6. Make sure you have node6 installed before running npm
install

```
> npm install
```

## Run

Will run the Typescript compiler, package the Angular2 application, watch for
changes, raise a development server and open the app on a browser tab. 

```
> npm start
```

(No tests yet)